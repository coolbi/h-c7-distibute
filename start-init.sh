#!/bin/bash

cp ./config/hadoop01/hadoop/* $HADOOP_HOME/etc/hadoop/
cp ./config/hadoop01/hbase/* $HBASE_HOME/conf/
cp ./config/hadoop01/hive/* $HIVE_HOME/conf/
cp ./config/hadoop01/spark/* /usr/local/spark/conf
cp ./config/hadoop01/zookeeper/* /usr/local/zookeeper/conf


scp -r ./config/hadoop02/hadoop/* hadoop02:$HADOOP_HOME/etc/hadoop/
scp -r ./config/hadoop02/hbase/* hadoop02:$HBASE_HOME/conf/
scp -r ./config/hadoop02/spark/* hadoop02:/usr/local/spark/conf

scp -r ./config/hadoop03/hadoop/* hadoop03:$HADOOP_HOME/etc/hadoop/
scp -r ./config/hadoop03/hbase/* hadoop03:$HBASE_HOME/conf/
scp -r ./config/hadoop03/spark/* hadoop03:/usr/local/spark/conf

cd /usr/local/zookeeper/bin/;./zkServer.sh start

source /etc/profile
rm -rf ~/.bash_history
history -c

rm -rf /opt/*